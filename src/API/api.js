
const baseAPI = 'http://10.0.0.2:3001';

export const postPhotofetch = (photo) => {
    return fetch(baseAPI+'/image',{
        method: 'POST',
        body: photo
    });
}