import { createStore , combineReducers , applyMiddleware, compose } from 'redux';

import postReducer from './reducers/postReducer';
import confReducer from './reducers/confReducer'
import profileReducer from './reducers/profileReducer';
import registerFormReducer from './reducers/registerPageReducer'
import loaderReducer from './reducers/loaderReducer';
import notify from './reducers/notifyReducer';

import reduxImmutableStateInvariant from 'redux-immutable-state-invariant';
import history from './history'
import { routerMiddleware, routerReducer, connectRouter} from 'connected-react-router'
import { responsiveStateReducer, responsiveStoreEnhancer} from 'redux-responsive'
import thunk from "redux-thunk";
const reducer = combineReducers({
    posts : postReducer,
    profile : profileReducer,
    register : registerFormReducer,
    loader: loaderReducer,
    device: responsiveStateReducer,
    confs: confReducer,
    notify
});
    
const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

const store = createStore(
    connectRouter(history)(reducer),
    composeEnhancers(
        responsiveStoreEnhancer,
        applyMiddleware(thunk, routerMiddleware(history),reduxImmutableStateInvariant())
    )
);

export default store;

