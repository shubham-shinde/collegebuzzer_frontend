import * as types from './../type/message';

const initialState = {
    mail: '',
    year: '',
    type: '',
    section: '',
    rollno: '',
    branch: '',
    gender: '',
    f_name: '',
    m_name: '',
    l_name: '',
    b_date: '',
    b_month: '',
    b_year: '',
    h_town: '',
    m_no: ''
};

export default (state=initialState, action) => {
    switch (action.type) {
        case types.UPDATE_ONCHANGE_REGISTER_FORM:
        {
            const newState = {...state}
            const { field, data } = action.payload
            newState[field] = data
            return newState
            break;
        }
        case types.TEXT_TO_SHOW_ON_DONE:
        {
            const newState = {
                text: action.payload
            }
            return newState;
            break;
        }
    
        default:
        {
            return state;
        }
            break;
    }
}