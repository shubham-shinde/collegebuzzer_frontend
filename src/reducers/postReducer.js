import * as types from './../type/message';
const initial_state =  []

export default (state = initial_state, action) => {
    switch (action.type) {
        case types.TOGGLE_ON_CLICK:{
            return state;
            break;
        }
        case types.UPDATE_POST_REDUCER: {
            let newState = [...state]
            newState = [...newState,...action.payload.posts]
            return newState;
            break;
        }
        default: {
            return state;
        }
            break;
    }
}
