import * as types from './../type/message';

const initialState = {
    pageXOffset:0
};

export default (state=initialState, action) => {
    switch (action.type) {
        case types.USER_SCROLLED:
        {
            return {
                pageXOffset: action.payload
            }
            break;
        }
            
            
    
        default:
        {
            return state;
            break;
        }
    }
}