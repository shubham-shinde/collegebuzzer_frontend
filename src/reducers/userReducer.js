import * as types from './../type/message';
const initial_state =  {
    f_name: '',
    m_name: '',
    l_name: '',
    p_pic: '',
    role: '',
    Id: '',
    clubs: []
}    

export default (state = initial_state, action) => {
    switch (action.type) {
        case types.TOGGLE_ON_CLICK:{
            return state;
            break;
        }
        case types.UPDATE_POST_REDUCER: {
            const newState = {...state}
            newState.posts = [...newState.posts,...action.payload.posts]
            return newState;
            break;
        }
        default: {
            return state;
        }
            break;
    }
}
