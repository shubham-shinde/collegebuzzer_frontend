import React from 'react';
import { connect } from 'react-redux';
import {bindActionCreators} from 'redux';
import * as action from './../../actions/actions';
import './posts.css';
import { Segment, Image, Icon } from 'semantic-ui-react';
import LazyLoadImage from '../lazyImageLoad/lazyload'
import Slider from 'react-slick'

import './slick-theme.css'
import './slick.css'

class Post extends React.Component{
    constructor(props) {
        super(props)
        this.state = {
            like : null
        }
    }
    like = () => {
        this.props.likeAPost(this.props._id);
        this.setState({
            like: 'red'
        })
    }
    flex(pics) {
        const bk = () => {
            return pics.map((dat,index) => {
                return(
                    <div key={index}>
                         {/* <img src={dat}/> */}
                        {/* <img src={image} /> */}
                        <p className="legend">
                        <LazyLoadImage srcPreload={dat.pre} srcLoaded={dat.pic} onScroll={(e) => console.log(e)}/>
                        </p>
                    </div>
                )
            })
        }
        if(pics) {
            const settings = {
                dots: true,
                infinite: false,
                auto: false,
                speed: 500,
                arrows: false,
                slidesToShow: 1,
                slidesToScroll: 1
            };
            if(pics.length === 1) {
                return(
                    <Slider {...settings}>
                        {bk()}
                    </Slider>
                )
            }
            return(
                <Slider {...settings}>
                    {bk()}
                </Slider>
            )
        }
        else {
            return null
        }
    }
    render() {
        console.log(this.state)
        const {key, p_pic, pics, f_name, m_name, l_name, userIntro, text, likes } = this.props;
        return (
            <Segment key={key} style={{marginTop: '10px'}}>

                <div style={{display:'flex',alignItems:'center'}}>
                    <div>
                        <Image circular src={p_pic} size="tiny"/>
                    </div>
                    <div style={{paddingLeft: '20px'}}>
                        <h3>{f_name} {m_name} {l_name}</h3>
                        <p>{userIntro}</p>
                    </div>
                </div>

                <div className="content">
                    <div>
                        {text}
                    </div>
                </div>

                <div className="img">
                        {this.flex(pics)}
                </div>
                <div style={{display: 'flex',justifyContent: 'space-around', marginTop:'10px'}} onClick={this.like}>
                    <Icon name="heart outline" size="large"/>
                    <div><span>{likes.length}</span> likes</div>
                </div>
                {/* <div className="comment_grid">
                    <div className="prv_comment" ref="comment">
                        <CommentsOfPosts comments={data.comments} visibility={this.state.commentVisibility} data={data}/>
                    </div>   
                    <div className="new_comment">
                        <div className="new_comment_grid_container">
                            <div className="comment_input" contentEditable="true"></div>
                            <div className="comment_button">
                                comment
                            </div>
                        </div>
                    </div>
                </div> */}
            </Segment>
        )
    }
}

export default Post;


// class ReplysForComments extends React.Component{
//     constructor(props) {
//         super(props);
//         this.state = {
//             visibility: 0
//         }
//     }
//     openAllReplys() {
//         const newState = Object.assign({}, this.state);
//         newState.visibility = !newState.visibility;
//         this.setState(newState);
//     }
//     showAllReply(data, index) {
//         return(
//             <div key={index} className="prv_comments_all_replys">
//                 <div className="prv_comments_all_replys_name">
//                     <span>
//                         {data.firstName} {data.surname} 
//                     </span>
//                     <span>
//                         {data.branch} {data.year}nd year
//                     </span>
//                 </div >
//                 <div className="prv_comments_all_replys_rply">{data.reply}</div>            
//             </div>
//         )
//     }
//     render() {
//         let rply = null;
//         if (this.state.visibility) {
//             rply = this.props.data.map(this.showAllReply.bind(this))
//         }
//          return(
//             <div>
//                 <div onClick={this.openAllReplys.bind(this)}  style={{color: 'blue'}}>show all Reply</div>                
//                 {rply}
//             </div>
//         );
//     }
// }

// class CommentsOfPosts extends React.Component{
//     constructor(props) {
//         super(props);
//         this.state = {
//             visibility: 0
//         }
//     }
//     openAllComments() {
//         const newState = Object.assign({}, this.state);
//         newState.visibility = !newState.visibility;
//         this.setState(newState);
//     }
//     showAllComments(comment, index) {
//         return (
//             <div key={index} className="comment_grid_container">
//                 <div className="prv_comment_name">
//                     <span>
//                     {comment.firstName} {comment.surname} 
//                     </span>
//                     <span>
//                         {comment.branch} {comment.year}nd year
//                     </span>
//                 </div>
//                 <div className="prv_comment_comment"><div>{comment.comment}</div></div>
//                 <div className="prv_comment_replys">
//                     <ReplysForComments data={comment.reply}/>
//                     <div className="your_rply">
//                         <div className="prv_comment_input" contentEditable="true"></div>
//                         <div className="prv_comment_reply_button">reply</div>
//                     </div>
//                 </div>
//             </div>
//         )
//     }
//     render() {
//         let cmt = null;
//         if (this.state.visibility) {
//             cmt = this.props.comments.map(this.showAllComments.bind(this));
//         }
//         return (
//             <div>
//                 <div className="likes_and_shares">
//                     <div>{this.props.data.likes} Likes & {this.props.data.shares} shares</div>
//                     <div onClick={this.openAllComments.bind(this)} style={{color:"blue"}}>comments</div>
//                 </div>
//                 {cmt}
//             </div>
//         );
//     }
// }

