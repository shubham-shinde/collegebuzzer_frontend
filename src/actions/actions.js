import * as types from './../type/message';
import {push} from 'connected-react-router';
import API from '../api/'
import {SERVER_URL} from '../app-config'

export const toggleAction = () => ({
    type : types.TOGGLE_ON_CLICK
})
export const redirectTo = (url) => {
    return push(url);
}

//loader
export const bringLoader = () => ({
    type : types.BRING_ON_LOADER
})
export const removeLoader = () => ({
    type : types.FUCK_THE_LOADER
})

//onscroll
export const userScrolled = (payload) => ({
    type : types.USER_SCROLLED,
    payload
})

export const getAllPosts = (id) => (dispatch) => {
    API().get("/getposts/"+id).then((data) => {
        console.log(data)
        dispatch({
            type: types.UPDATE_POST_REDUCER,
            payload: data
        })    
    })    
}
export const likeAPost = (id) => {
    return API().get("/likepost/"+id) 
}
export const getAllConfs = (id) => (dispatch) => {
    API().get("/getconfs/"+id).then((data) => {
        console.log(data)
        dispatch({
            type: types.UPDATE_CONF_REDUCER,
            payload: data
        })    
    })    
}