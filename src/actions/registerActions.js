import * as types from './../type/message';
import * as commonActions from './actions'
import fetch from './../api/';

export const changeRegisterFrom = (payload) => {
    return {
        type: types.UPDATE_ONCHANGE_REGISTER_FORM,
        payload
    }
}

export const thisGayIsCompletedForm = () => {
    return (dispatch, getState) => {
        const form = {...getState().register}
        dispatch(commonActions.bringLoader());
        form['dob'] = new Date([form.b_year, form.b_month, form.b_date].join('-'))
        fetch().post('/student/register', form).then((data) => {

            console.log(data);
            if(data.msg) {
                dispatch(textToShowOnDone(data.msg))
            }
            else {
                dispatch(textToShowOnDone('Error'))
            }
            dispatch(commonActions.removeLoader())

        }).catch((err) => {

            console.log(err);
            dispatch(textToShowOnDone(err.toString()))
            dispatch(commonActions.removeLoader())

        })
    }
}

export const textToShowOnDone = (payload) => ({
    type: types.TEXT_TO_SHOW_ON_DONE,
    payload
})

export const clickOnDone = () => (dispatch) => {
    return dispatch(commonActions.redirectTo('/login'));
}