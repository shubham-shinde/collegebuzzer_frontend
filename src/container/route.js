import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {Router, Switch, Route} from 'react-router-dom';
import history from './../history';

import Post from './postsLayout/posts';
import Photo from './photo';
import Register from './auth/register/'
import AddBio from './auth/addbio'
import AddPost from './postsLayout/addpost'
import Login from './auth/login/'
import Header from './header/header';
import Profile from './profile/profile';
import Confessions from './confessionCont/';
import AddConf from './confessionCont/addConfessionCont'


import NotificationService from './notification/notify';
import Loader from './loader/'

class Root extends Component {
    render() {
        return (  
        <div>
            <Loader/>
            <Header>
            <Switch>
                <Route exact path="/" component={Post}/>
                <Route path="/photo" component={Photo}/>
                <Route path="/register" component={Register}/>
                <Route path="/addBio" component={AddBio}/>
                <Route path="/addPost" component={AddPost}/>
                <Route path="/login" component={Login}/>
                <Route path="/profile" component={Profile}/>
                <Route path="/confessions" component={Confessions}/>
                <Route path="/addConfession" component={AddConf}/>
            </Switch>
            </Header>
        </div>
        );
    }
}

// App.propTypes = {
//   story: PropTypes.object.isRequired,
//   history: PropTypes.object.isRequired
// }

export default Root;
