import React from 'react';
import { connect } from 'react-redux';
import {bindActionCreators} from 'redux';
import * as action from './../../actions/actions';
import Confession from '../../components/confessionsComponent/';
import { Segment, Button } from 'semantic-ui-react'

const style = {
    div:{
        position: 'relative',
        boxShadow: '0 1px 2px 0 rgba(34,36,38,.15)',
        margin: '1rem 0',
        padding: '1em',
        borderRadius: '.28571429rem',
        fontSize: '2rem',
        textAlign: 'center',
        height: '350px',
        letterSpacing: '1px',
        wordSpacing: '10px',
        lineHeight: '40px',
        fontWeight: '800',
        verticalAlign: 'middle',
        overflow: 'hidden'
    },
    p: {
        position: 'absolute',
        left: '50%',
        top: '50%',
        transform: 'translate(-50%,-50%)'
    }
}
const theme = {
    sunset : {
        background: 'linear-gradient(to right,#0b486b,#f56217)',
        color: 'white',
    },
    kingYan : {
        background: 'linear-gradient(to right,#1a2a6c,#b21f1f,#fdbb2d)',
        color: 'white',
    },
    rainbowBlue : {
        background: 'linear-gradient(to right,#00f260,#0575e6)',
        color: 'white',
    },
    darkGray : {         
        background: 'linear-gradient(to right,#0f2027,#2c5364,#203a43)',
        color: 'white',
    },
    deepSpace : {
        background: 'linear-gradient(to right,#000000,#434343)',
        color: 'white',
    },
    scooter : {
        background: 'linear-gradient(to right,#36d1dc,#5b86e5)',
        color: 'white',
    },
    amin : {
        background: 'linear-gradient(to right,#8e2de2,#4a00e0)',
        color: 'white',
    },
    delicate : {
        background: 'linear-gradient(to right,#d3cce3,#e9e4f0)',
        color: 'black'
    }
}

class App extends React.Component{
    constructor(props) {
        super(props);
        this.state = theme['sunset']
    }
    changeTheme (newtheme) {
        const newState = theme[newtheme]
        this.setState(newState);
    }
    render() {
        return (
            <Segment>
                <div contentEditable="true" style={{...style.div,...this.state}}>

                </div>
                <div style={{display:'flex', justifyContent:'space-between'}}>
                    <div style={{width: '48%'}}>
                    <Button fluid style={{backgroundColor: '#0b486b',color: 'white', margin: '5px 0'}} onClick={() => this.changeTheme('sunset')}>sunset</Button>
                    <Button fluid style={{backgroundColor: '#1a2a6c',color: 'white', margin: '5px 0'}} onClick={() => this.changeTheme('kingYan')}>kingYan</Button>
                    <Button fluid style={{backgroundColor: '#00f260',color: 'white', margin: '5px 0'}} onClick={() => this.changeTheme('rainbowBlue')}>rainbowBlue</Button>
                    <Button fluid style={{backgroundColor: '#0f2027',color: 'white', margin: '5px 0'}} onClick={() => this.changeTheme('darkGray')}>darkGray</Button>
                    </div>
                    <div style={{width: '48%'}}>
                    <Button fluid style={{backgroundColor: '#000000',color: 'white', margin: '5px 0'}} onClick={() => this.changeTheme('deepSpace')}>deepSpace</Button>
                    <Button fluid style={{backgroundColor: '#36d1dc',color: 'white', margin: '5px 0'}} onClick={() => this.changeTheme('scooter')}>scooter</Button>
                    <Button fluid style={{backgroundColor: '#8e2de2',color: 'white', margin: '5px 0'}} onClick={() => this.changeTheme('amin')}>amin</Button>
                    <Button fluid style={{backgroundColor: '#d3cce3',color: 'black', margin: '5px 0'}} onClick={() => this.changeTheme('delicate')}>delicate</Button>
                    </div>
                </div>
                <Button fluid color='blue'>CONFIRM CONFESSION</Button>
            </Segment>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        posts : state.posts
    }
}

const matchDispatchToProps = (dispatch) => {
    return bindActionCreators(action, dispatch);
}

export default connect(mapStateToProps, matchDispatchToProps)(App);

