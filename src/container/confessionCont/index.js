import React from 'react';
import { connect } from 'react-redux';
import {bindActionCreators} from 'redux';
import * as action from './../../actions/actions';
import Confession from '../../components/confessionsComponent/';

class App extends React.Component{
    componentWillMount = () => {
        if(this.props.confs.length === 0)
            this.props.getAllConfs(0)
    }
    loadmore = () => {
        const confs = this.props.confs
        const lastconfId = confs[confs.length - 1]._id
        this.props.getAllConfs(lastconfId)
    }
    render() {
        const conf = this.props.confs.map((text,index) => (
                <Confession key={index} data={text}/>
            )
        );
        return (
            <div>
                {conf}
                <button onClick={this.loadmore}>load more</button>
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        confs : state.confs
    }
}

const matchDispatchToProps = (dispatch) => {
    return bindActionCreators(action, dispatch);
}

export default connect(mapStateToProps, matchDispatchToProps)(App);

