import React from 'react';
import { connect } from 'react-redux';
import {bindActionCreators} from 'redux';
import * as action from './../../actions/actions';
import { Form, Button , Segment, TextArea} from 'semantic-ui-react'
import Post from '../../components/postComponent/';

class AddPost extends React.Component{
    constructor(props) {
        super(props);
    }
    render() {
        const post = this.props.posts.posts.map((data) => (
                <Post key={data.key} p_pic={data.p_pic} pics={data.pics} f_name={data.f_name} m_name={data.m_name} s_name={data.s_name} userIntro={data.userIntro} text={data.text} likes={data.likes}/>
            )
        );
        return (
            <div>
                <form method="POST" action="/addPost" enctype="multipart/form-data" id='addPostForm'>
                    <div class="field">
                        <textarea name="text" id="" cols="30" rows="10" form="addPostForm" ></textarea>
                        <label for="image">Image Upload</label>
                        <input type="file" name="image" id="image"/>
                    </div>
                    <input type="submit" class="btn" value="Save"/>
                </form>
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        posts : state.posts
    }
}

const matchDispatchToProps = (dispatch) => {
    return bindActionCreators(action, dispatch);
}

export default connect(mapStateToProps, matchDispatchToProps)(AddPost);

