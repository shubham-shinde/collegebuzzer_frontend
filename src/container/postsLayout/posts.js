import React from 'react';
import { connect } from 'react-redux';
import {bindActionCreators} from 'redux';
import * as action from './../../actions/actions';
import Post from '../../components/postComponent/';

class App extends React.Component{
    constructor(props) {
        super(props);
    }
    componentWillMount = () => {
        if(this.props.posts.length === 0)
            this.props.getAllPosts(0)
    }
    loadmore = () => {
        const posts = this.props.posts
        const lastpostId = posts[posts.length - 1].post._id
        this.props.getAllPosts(lastpostId)
    }
    render() {
        const post = this.props.posts.map((data) => (
                <Post key={data.post._id} _id={data.post._id} p_pic={data.post.p_pic} pics={data.pics} f_name={data.post.f_name} m_name={data.post.m_name} l_name={data.post.l_name} userIntro={data.post.userIntro} text={data.post.text} likes={data.post.likes}/>
            )
        );
        return (
            <div>
                {post}
                <button onClick={this.loadmore}>load more</button>
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        posts : state.posts
    }
}

const matchDispatchToProps = (dispatch) => {
    return bindActionCreators(action, dispatch);
}

export default connect(mapStateToProps, matchDispatchToProps)(App);

