import React from 'react';
import './sidebar_left.css';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import { Segment, Image, Button } from 'semantic-ui-react'
import * as action from './../../../actions/actions';

class SidebarLeft extends React.Component{
    constructor(props) {
        super(props);
        this.state = {};
    }
    routeTo = (route) => () => {
        this.props.redirectTo(route)
    } 
    render() {
        return(
            <div style={{padding:'40px 0'}}>
            <center>
                <Segment circular="true" textAlign="center"> 
                    <Image src='http://placehold.it/150x150' circular size='small' />
                </Segment>
                <div className="name"><h1 style={{padding: '5px'}}>kuch bhi</h1></div>
                <div className="intro" style={{padding: '5px'}}>4th year EEE</div>
                <div style={{padding: '5px'}}><Button style={{width:'80%'}} onClick={this.routeTo('/profile')}>View profile</Button></div>
                <div style={{padding: '5px'}}><Button style={{width:'80%'}} onClick={this.routeTo('/clubs')}>Clubs</Button></div>
                <div style={{padding: '5px'}}><Button style={{width:'80%'}} onClick={this.routeTo('/addPost')}>Add post</Button></div>
                <div style={{padding: '5px'}}><Button style={{width:'80%'}} onClick={this.routeTo('/addconfession')}>Write Confession</Button></div>
                <div style={{padding: '5px'}}><Button style={{width:'80%'}} onClick={this.routeTo('/editprofile')}>Edit profile</Button></div>
                <div style={{padding: '5px'}}><Button style={{width:'80%'}} onClick={this.routeTo('/logout')}>Logout</Button></div>
                <button onClick={this.routeTo('/register')}>signup</button>
                <button onClick={this.routeTo('/login')}>login</button>
                <button onClick={this.routeTo('/addBio')}>addBio</button>
            </center>
            </div>
        )
    }
}
const mapStateToProps = (state) => {
    return {
        loader : state.loader
    }
}

const matchDispatchToProps = (dispatch) => {
    return bindActionCreators(action, dispatch);
}

export default connect(mapStateToProps, matchDispatchToProps)(SidebarLeft);