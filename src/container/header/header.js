import React from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import './header.css';

import SidebarLeft from './sidebar_left/sidebar_left';
import Search from './search'
import Notify from './notification';

import * as action from './../../actions/actions'
import Headroom from 'react-headroom'
import { Icon, Grid, Segment, Button } from 'semantic-ui-react';

console.log(window);
class Header extends React.Component{
    constructor(props) {
        super(props);
        this.state = {
            search: {
                display: 'none',
                active: 'false',
                color: 'transparent'
            },
            notify: {
                display: 'none',
                active: 'false',
                color: 'transparent'
            }
        }
    }

    OpenDiv = (element, reverse) => {
        console.log(element)
        const newState = {...this.state}

        newState[reverse].display = 'none' 
        newState[reverse].color = 'transparent' 
        newState[reverse].active = false
       
        newState[element].display = (newState[element].active) ? 'none':'block';
        newState[element].color = (newState[element].active) ? 'transparent':'orange';
        newState[element].active = (newState[element].active) ? false:true;
        this.setState(newState);
    }

    render() {
        const small = this.props.device.greaterThan.small;
        if(small) {
            return(
                <div>
                    <div className="headerlpy">
                        header
                    </div>
                    <div className="threeDivslpy">
                        <div>
                            <div className="fixedply">
                            <Segment>
                            <SidebarLeft/>
                            </Segment>
                            </div>
                        </div>
                        <div className="dataDisplaylpy">
                        {this.props.children}
                        </div>
                        <div>
                            <div className="fixedply">
                            <Button fluid onClick={() => this.props.redirectTo('/confessions')}>
                                <h1>Confessions</h1>
                            </Button>
                            <Button style={{marginTop: '20px'}} fluid onClick={() => this.props.redirectTo('/')}>
                                <h1>Home</h1>
                            </Button>
                            <Segment color='blue'>
                                <Search/>
                            </Segment>
                            {/* <Segment color='red' style={{overflow: 'scroll'}}>
                                <Notify/>
                            </Segment> */}
                            </div>
                        </div>
                    </div>
                </div>
            )
        }
        return(
            <div>
            <Headroom style={{
                webkitTransition: 'all .5s ease-in-out',
                mozTransition: 'all .5s ease-in-out',
                oTransition: 'all .5s ease-in-out',
                transition: 'all .5s ease-in-out',
                position: "fixed"
            }}>
            <div>
                <input type="checkbox" id="sidebar_toggel_checkbox_left"/>
                <div id='header_and_sidebar'>                    
                        <div id="sidebar_div_left">
                            <SidebarLeft/>
                        </div>
                        <div id="header">
                            <div className="mainHeader">
                                <div>                                
                                    <label htmlFor="sidebar_toggel_checkbox_left" id="sidebar_toggel_checkbox_label_left">
                                        <Icon name='align justify' size='big'/>
                                    </label>
                                </div>
                                <div className="headingOrLogo">
        
                                </div>
                                <div onClick={() => this.props.redirectTo('/confessions')}>                                
                                    <Icon name='copyright outline' size='big'/>                             
                                </div>
                                <div style={{backgroundColor: this.state.search.color}}>                                
                                    <Icon name='search' size='big' onClick={() => this.OpenDiv('search','notify')}/>                                     
                                <div className='searchDiv openOnClick' style={{display: this.state.search.display}}>
                                    <Search/>
                                </div>                               
                                </div>
                                {/* <div style={{backgroundColor: this.state.notify.color}}>                                
                                <Icon name='bell outline' size='big' onClick={() => this.OpenDiv('notify','search')}/>                                     
                                <div className='notfyDiv openOnClick' style={{display: this.state.notify.display}}>
                                <Segment style={{padding: '10px 40px',}}>
                                    <Notify/>
                                </Segment>
                                </div>                               
                                </div> */}
                            </div>
                        </div>
                </div>
            </div>
            </Headroom>
            {this.props.children}
            </div>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        device: state.device
    }
}

const matchDispatchToProps = (dispatch) => {
    return bindActionCreators(action, dispatch);
}

export default connect(mapStateToProps, matchDispatchToProps)(Header);