import React from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import * as action from './../../actions/actions'
import { Input, Segment } from 'semantic-ui-react';

class Search extends React.Component{
    constructor(props) {
        super(props);
        this.state = {}
    }

    render() {
        const small = this.props.device.greaterThan.small;
        return(
            <div>
                <Input loading placeholder='Search...' fluid/>
            </div>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        device: state.device
    }
}

const matchDispatchToProps = (dispatch) => {
    return bindActionCreators(action, dispatch);
}

export default connect(mapStateToProps, matchDispatchToProps)(Search);