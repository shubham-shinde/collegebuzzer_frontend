import React from 'react';
import { ConnectedRouter } from 'connected-react-router';
import Route from './route';
import { Provider } from 'react-redux';


class Root extends React.Component {
    render() {
        return(
            <Provider store = {this.props.store}>
                <ConnectedRouter history={this.props.history}>
                    <Route />
                </ConnectedRouter>
            </Provider>
        )
    }
}

export default Root;