import React from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import * as action from './../../../actions/actions.js';
class Login extends React.Component{
    constructor(props) {
        super(props);
        this.state = {};
    }
    render() {
        console.log(this.props)
        return(
            <div>
                <h1>Login Page</h1>
                <input type="text" placeholder="email id"/>
                <input type="password" placeholder="Password"/>                
                <button>submit</button>
            </div>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        login : state.login
    }
}

const matchDispatchToProps = (dispatch) => {
    return bindActionCreators(action, dispatch);
}

export default connect(mapStateToProps, matchDispatchToProps)(Login);