import React from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import * as action from './../../../actions/registerActions';
import { Icon, Input,Form, Select } from 'semantic-ui-react'
import './style.css';


class Register extends React.Component{
    constructor(props) {
        super(props);
        this.state = {
          bodyStyle: {height: '100vh', width: '100vw', backgroundColor: '#251605'},
          accountCSS: {display: 'block'},
          socialCSS: {display: 'none'},
          detailsCSS: {display: 'none'},
          doneCSS: {display: 'none'},
          accountSClass: 'ui big basic violet button',
          socialPClass: 'ui big basic blue button disabled',
          detailClass: 'ui big basic orange button disabled',
          doneClass: 'ui big basic orange button disabled',

        };
    }
    submit = (e) => {
        e.preventDefault()
        this.props.thisGayIsCompletedForm();
        const newState = {...this.state}
        newState.bodyStyle = {height: '100vh', width: '100vw', backgroundColor: '#0009ff96'}
        newState.detailsCSS = {display: 'none'}
        newState.doneCSS = {display: 'block'}
        newState.detailClass = 'ui big basic violet button disabled'
        newState.doneClass = 'ui big basic blue button'
        this.setState(newState);
    }
    clickDone = () => {
      this.props.clickOnDone();
    }
    onFormChange=(e, obj) => {
      console.log(e.target.selected);
      console.log(e.target)
      if(obj) {
        this.props.changeRegisterFrom({
          field: obj['data-id'],
          data: obj.value
        })
      }
      else {
        this.props.changeRegisterFrom({
          field: e.target.parentElement.getAttribute('data-id'),
          data: e.target.value
        })
      }
      

    }
    next1 = (e) => {
      e.preventDefault();
      const newState = {...this.state}
      newState.bodyStyle = {height: '100vh', width: '100vw', backgroundColor: '#06000a'}
      newState.accountCSS = {display: 'none'}
      newState.socialCSS = {display: 'block'}
      newState.accountSClass = 'ui big basic violet button disabled'
      newState.socialPClass = 'ui big basic blue button'
      this.setState(newState);
    }
    next2 = (e) => {
      e.preventDefault();
      const newState = {...this.state}
      newState.bodyStyle = {height: '100vh', width: '100vw', backgroundColor: '#300032'}
      newState.socialCSS = {display: 'none'}
      newState.detailsCSS = {display: 'block'}
      newState.detailClass = 'ui big basic orange button'
      newState.socialPClass = 'ui big basic blue button disabled'
      this.setState(newState);
    }
    prev1 = (e) => {
      e.preventDefault();
      const newState = {...this.state}
      newState.bodyStyle = {height: '100vh', width: '100vw', backgroundColor: '#251605'}
      newState.accountCSS = {display: 'block'}
      newState.socialCSS = {display: 'none'}
      newState.accountSClass = 'ui big basic violet button'
      newState.socialPClass = 'ui big basic blue button disabled'
      this.setState(newState);
    }    
    prev2 = (e) => {
      e.preventDefault();
      const newState = {...this.state}
      newState.bodyStyle = {height: '100vh', width: '100vw', backgroundColor: '#06000a'}
      newState.socialCSS = {display: 'block'}
      newState.detailsCSS = {display: 'none'}
      newState.detailClass = 'ui big basic orange button disabled'
      newState.socialPClass = 'ui big basic blue button'
      this.setState(newState);
    }
    // var ct = 0;
    // $('.next1').one('click', function(e) {
    //   e.preventDefault();
    //   $('#account').animate('slow', function() {
    //     if (ct > 0) {
    //       $('#account').removeClass('transition visible');
    //       $('#account').addClass('transition hidden');
    //     }
    //     $('#account').css('display', 'none');
    //     $('#accountS').addClass('disabled');
    //     $('#socialP').removeClass('disabled');
    //     $("#social").transition('fly right');
    //     $('body').css('background-color', '#06000a');
    //     $('#social button').removeClass('inverted violet');
    //     $('#social button').addClass('inverted blue');
    //     ct++;
    //   });
    // });
    // $('.prev1').one('click', function(e) {
    //   e.preventDefault();
    //   $('#accountS').removeClass('disabled');
    //   $('#socialP').addClass('disabled');
    //   $('#social').animate('slow', function() {
    //     $('body').css('background-color', '#300032');
    //     $('#social').transition('hide');
    //     $("#account").transition('fly right');
    //   });
    // });
    // $('.next2').one('click', function(m) {
    //   m.preventDefault();
    //   $('#socialP').addClass('disabled');
    //   $('#details').removeClass('disabled');
    //   $('#social').animate('slow', function() {
    //     $('#personal button').removeClass("inverted violet");
    //     $('#personal button').addClass("inverted orange");
    //     $('body').css('background-color', '#251605');
    //     $('#social').transition('hide');
    //     $('#personal').transition('fly right');
    //   });
    // });
    // $('.prev2').one('click', function(m) {
    //   m.preventDefault();
    //   $('#details').addClass('disabled');
    //   $('#socialP').removeClass('disabled');
    //   $('#personal').animate('slow', function() {
    //     $('body').css('background-color', '#06000a');
    //     $('#personal').transition('hide');
    //     $('#social').transition('fly right');
    //   });
    // });
    // $('.submit').one('click', function(p) {
    //   p.preventDefault();
    //   $('#personal').stop();
    // });











    render() {
      return(
        <div style={this.state.bodyStyle}>
        <link href="https://fonts.googleapis.com/css?family=Lobster" rel="stylesheet" type="text/css" />
        <div className="ui centered  grid container">
          <div className="row" />
          <div className="row" />
          <div className="row" />
          <div className="ui text container">
            <div className="four ui  buttons center aligned grid container" style={{margin: 20}}>
              <div className={this.state.accountSClass} id="accountS">
                <div className="content" style={{fontSize: 12, color: 'white'}}>ACCOUNT SETUP</div>
              </div>
              <button className={this.state.socialPClass} id="socialP">
                <div className="content" style={{fontSize: 12, color: 'white'}}>PERSONAL DETAILS</div>
              </button>
              <button className={this.state.detailClass} id="details">
                <div className="content" style={{fontSize: 12, color: 'white'}}>COLLAGE DETAILS</div>
              </button>
              <button className={this.state.doneClass} id="details">
                <div className="content" style={{fontSize: 12, color: 'white'}}>DONE</div>
              </button>
            </div>
            <div className="row" />
            <div className="row" />
            <div id="account" className="main3div" style={this.state.accountCSS}>
              <div className="ui center aligned  segment container " id="signUpBox" style={{backgroundColor: '#F1F0FF', borderRadius: 5}}>
                <div className="ui centered header">
                  <h1 className="font" style={{color: '#300032'}}>Create your account</h1>
                </div>
                <Form className="ui form">
                  <Form.Input onChange={this.onFormChange} data-id="mail" icon="mail" iconPosition='left' placeholder="Email(xyz@email.com)" width={16}/>
                  <Form.Input onChange={this.onFormChange} data-id="m_no" icon="phone" iconPosition='left' placeholder="Phone Number" width={16}/>                  
                  <div className="one ui buttons">
                    <button className="ui  inverted violet  medium button next1" onClick={this.next1}>Next</button>
                  </div>
                </Form>
              </div>
            </div>
            <div id="social" className="main3div" style={this.state.socialCSS}>
              <div className="ui center aligned  segment container " id="signUpBox" style={{backgroundColor: '#F1F0FF', borderRadius: 5}}>
                <div className="ui centered header">
                  <h1 className="font" style={{color: 'rgb(50,153,153)'}}>Personal Details</h1>
                </div>
                <Form className="ui form">
                  <Form.Group>
                    <Form.Input onChange={this.onFormChange} data-id="f_name" icon='user' iconPosition='left' placeholder='First Name' width={6}/>
                    <Form.Input onChange={this.onFormChange} data-id="m_name" placeholder='Middle Name(optinal)' width={4}/>
                    <Form.Input onChange={this.onFormChange} data-id="l_name" placeholder='Last Name' width={6}/>
                  </Form.Group>
                  <Form.Select onChange={this.onFormChange} data-id="gender" options={[
                    { key: 'm', icon: 'male', text: 'Male', value: 'M' },
                    { key: 'f', icon: 'female', text: 'Female', value: 'F' }
                  ]} placeholder='Gender'/>
                  <Form.Group>
                    <Form.Input onChange={this.onFormChange} data-id="b_date" icon='birthday cake' iconPosition='left' placeholder='DD' width={3}/>
                    <Form.Select onChange={this.onFormChange} data-id="b_month" options={[
                      { key: 'ff1', text: 'January', value: 0 },
                      { key: 'ff2', text: 'February', value: 1 },
                      { key: 'ff3', text: 'March', value: 2 },
                      { key: 'ff4', text: 'April', value: 3 },
                      { key: 'ff5', text: 'May', value: 4 },
                      { key: 'ff6', text: 'June', value: 5 },
                      { key: 'ff7', text: 'July', value: 6 },
                      { key: 'ff8', text: 'August', value: 7 },
                      { key: 'ff9', text: 'September', value: 8 },
                      { key: 'ff10', text: 'October', value: 9 },
                      { key: 'ff11', text: 'November', value: 10 },
                      { key: 'ff12', text: 'December', value: 11 }
                    ]} placeholder='Month'/>
                    <Form.Input onChange={this.onFormChange} data-id="b_year" placeholder='YYYY' width={3}/>
                  </Form.Group>
                  <Form.Input onChange={this.onFormChange} data-id="h_town" icon='home' iconPosition='left' placeholder='Home Town' width={16}/>
                  <div className="two ui buttons">
                    <button className="ui  inverted violet  medium button prev1" onClick={this.prev1}>Previous</button>
                    <button className="ui  inverted violet  medium button next2" onClick={this.next2}>Next</button>
                  </div>
                </Form>
              </div>
            </div>
            <div id="personal" className="main3div" style={this.state.detailsCSS}>
              <div className="ui center aligned  segment container " id="signUpBox" style={{backgroundColor: '#F1F0FF', borderRadius: 5}}>
                <div className="ui centered header">
                  <h1 className="font" style={{color: 'orange'}}>Collage Details</h1>
                </div>
                <Form className="ui form">
                  <Form.Input onChange={this.onFormChange} data-id="rollno" icon="pencil alternate" iconPosition="left" placeholder="AKTU Roll Number" width={16}/>
                  <Form.Select onChange={this.onFormChange} data-id="year" options={[
                    { key: 'mf1', text: 'First', value: '1' },
                    { key: 'mf2', text: 'Second', value: '2' },
                    { key: 'mf3', text: 'Third', value: '3' },
                    { key: 'mf4', text: 'Fourth', value: '4' },
                  ]} placeholder='Year'/>
                  <Form.Select onChange={this.onFormChange} data-id="branch" options={[
                    { key: 'etIT', text: 'Information Technology', value: 'IT' },
                    { key: 'etCS', text: 'Computer science and Engineering', value: 'CS' },
                    { key: 'etME', text: 'Mechanical Engineering', value: 'ME' },
                    { key: 'etCE', text: 'Civil Engineering', value: 'CE' },
                    { key: 'etEC', text: 'Electronics and Communication Engineering', value: 'EC' },
                    { key: 'etE3', text: 'Electrical and Electronics Engineering', value: 'E3' },
                    { key: 'etEE', text: 'Electrical Engineering', value: 'EE' },
                    { key: 'etEI', text: 'Electronics and Instrumentation Engineering', value: 'EI' },
                  ]} placeholder='Branch'/>
                  <Form.Select onChange={this.onFormChange} data-id="section" options={[
                    { key: 'gf1', text: 'A', value: 'A' },
                    { key: 'gf2', text: 'B', value: 'B' },
                  ]} placeholder='Section'/>
                  <Form.Select onChange={this.onFormChange} data-id="type" options={[
                    { key: 'ps1', text: 'Regular', value: 'R' },
                    { key: 'ps2', text: 'Branch Change', value: 'B' },
                    { key: 'ps3', text: 'Lateral Entry', value: 'L' },
                  ]} placeholder='Type of Entry in Collage'/>
                  <div className="two ui buttons">
                    <button className="ui  inverted violet  medium button prev2" onClick={this.prev2}>Previous</button>
                    <button className="ui  inverted violet  medium button submit" onClick={this.submit}>Submit</button>
                  </div>
                </Form>
              </div>
            </div>
            <div id="done" className="main3div" style={this.state.doneCSS}>
              <div className="ui center aligned  segment container " id="signUpBox" style={{backgroundColor: '#F1F0FF', borderRadius: 5}}>
                <div className="ui centered header">
                  <h1 className="font" style={{color: 'orange'}}>DONE</h1>
                </div>
                  <Form className="ui form">
                    <h1>{this.props.register.text}</h1>
                    <div className="one ui buttons">
                      <button className="ui  inverted violet  medium button next1" onClick={this.clickDone}>Done</button>
                    </div>
                  </Form>
                
              </div>
            </div>
          </div>
        </div>
      </div>
      )
    }
}

const mapStateToProps = (state) => {
    return {
        register : state.register,
    }
}

const matchDispatchToProps = (dispatch) => {
    return bindActionCreators(action, dispatch);
}

export default connect(mapStateToProps, matchDispatchToProps)(Register);