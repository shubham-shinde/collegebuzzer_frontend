import React from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import * as action from './../../actions/actions';
class AddBio extends React.Component{
    constructor(props) {
        super(props);
        this.state = {};
    }
    render() {
        console.log(this.props)
        return(
            <div>
                <h1>ADD YOUR BIO</h1>
                <input type="text" placeholder="Bio"/>
                <button>submit</button>
            </div>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        addBio : state.addBio
    }
}

const matchDispatchToProps = (dispatch) => {
    return bindActionCreators(action, dispatch);
}

export default connect(mapStateToProps, matchDispatchToProps)(AddBio);