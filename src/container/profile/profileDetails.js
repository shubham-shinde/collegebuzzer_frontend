import React from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import moment from 'moment';
import * as action from './../../actions/actions';



class ProfileDetail extends React.Component {
    render() {
        const profile = this.props.data;
        return (
            <div id='profile_detail'>
                <div><i class="fas fa-heart"></i> single</div>
                <div><i class="fas fa-birthday-cake"></i> {moment(profile.bithday).format("MMM Do YY")}</div>
                <div><i className="fas fa-home"></i> {profile.hometown}</div>
                <div><i className="fas fa-building"></i> {profile.hostel}</div>    
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        profile : state.posts
    }
}

const matchDispatchToProps = (dispatch) => {
    return bindActionCreators(action, dispatch);
}

export default connect(mapStateToProps, matchDispatchToProps)(ProfileDetail);