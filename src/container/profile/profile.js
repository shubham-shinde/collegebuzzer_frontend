import React from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import * as action from './../../actions/actions';
import './profile.css';
import ProfileDetail from './profileDetails';
import Posts from './../postsLayout/posts';
import { Segment, Image, Icon } from 'semantic-ui-react'
class Profile extends React.Component{
    constructor(props) {
        super(props);
        this.state = {};
    }
    render() {
        console.log(this.props)
        const profile = this.props.profile.profile;
        return(
            <div id="profile">
                <center>
                    <div className='backGradient'>
                    <Segment circular>
                        <Image src={profile.pic} circular />
                    </Segment>
                    <h1>{profile.firstName} {profile.surname}</h1>
                    <div>{profile.branch} {profile.year}rd year</div>
                        <div id="his_qoute" style={{marginTop: '20px'}}>
                            <i class="fas fa-quote-left"></i>
                            <p style={{textAlign: 'center'}}>{profile.aboutHim}</p>
                            <i class="fas fa-quote-right"></i>
                        </div>
                    </div>
                    <Segment>
                        Posts
                    </Segment>
                </center>
                <Posts/>
            </div>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        profile : state.profile
    }
}

const matchDispatchToProps = (dispatch) => {
    return bindActionCreators(action, dispatch);
}

export default connect(mapStateToProps, matchDispatchToProps)(Profile);